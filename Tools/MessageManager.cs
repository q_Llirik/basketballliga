﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BasketBallLiga.Tools
{
    class MessageManager
    {
        static public void ShowGoodMessage(string message)
        {
            MessageBox.Show(message, App.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        static public void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, App.AppName + " - Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
