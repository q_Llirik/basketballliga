﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace BasketBallLiga.Tools
{
    public class Navigation
    {
        public static MainWindow main = null;
        
        static public void OpenPage(Page page)
        {
            if (main == null)
            {
                throw new NullReferenceException("Сначала необходимо задать main.");
            }
            main.Title = App.AppName + " - " + page.Title;
            main.Container.Navigate(page);
        } 
    }
}
