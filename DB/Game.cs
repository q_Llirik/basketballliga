﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketBallLiga.DB
{
    partial class Games
    {
        public string GetTeams
        {
            get
            {
                var ret = "";
                foreach(var team in TeamsInGame)
                {
                    ret += team.Teames.Name + "\n";
                }
                return ret;
            }
        }
    }
}
