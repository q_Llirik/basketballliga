﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddGamePage.xaml
    /// </summary>
    public partial class AddGamePage : Page
    {
        public AddGamePage()
        {
            InitializeComponent();

            dpStart.SelectedDate = DateTime.Now;
            dpEnd.SelectedDate = DateTime.Now;

            lvTeams.ItemsSource = DataBase.db.Teames.ToList();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new GamesPage());
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            var listTeams = (List<Teames>)lvTeams.ItemsSource;
            if (dpStart.SelectedDate.Value >= dpEnd.SelectedDate.Value)
            {
                MessageManager.ShowErrorMessage("Выберите корректную дату.");
                return;
            }
            
            if (listTeams.Where(w=>w.GetSelect == true).Count() <= 1)
            {
                MessageManager.ShowErrorMessage("Выберите хотя бы две команды.");
                return;
            }

            var game = new Games()
            {
                DateStart = dpStart.SelectedDate.Value,
                DateEnd = dpEnd.SelectedDate.Value
            };

            foreach(var team in listTeams.Where(w=>w.GetSelect == true))
            {
                DataBase.db.TeamsInGame.Add(new TeamsInGame() {
                    Games = game,
                    Teames = team
                });
            }

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            MessageManager.ShowGoodMessage("Соревнование создано.");
            Back_Click(null,null);
        }
    }
}
