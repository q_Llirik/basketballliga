﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для TeamesPage.xaml
    /// </summary>
    public partial class TeamesPage : Page
    {
        public TeamesPage()
        {
            InitializeComponent();

            UpDateListView();
        }

        private void UpDateListView()
        {
            lvTeames.Items.Clear();
            foreach (var team in DataBase.db.Teames.ToList())
                lvTeames.Items.Add(team);
        }

        private void AddNewTeam_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new AddNewTeamAndChangeTeamPage());
        }

        private void ChangeTeam_Click(object sender, RoutedEventArgs e)
        {
            if (lvTeames.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите изменяемую команду.");
                return;
            }

            Navigation.OpenPage(new AddNewTeamAndChangeTeamPage((Teames)lvTeames.SelectedItem));
        }

        private void DeleteTeam_Click(object sender, RoutedEventArgs e)
        {
            if (lvTeames.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите удаляемую команду.");
                return;
            }

            DataBase.db.Teames.Remove((Teames)lvTeames.SelectedItem);

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            UpDateListView();
            MessageManager.ShowGoodMessage("Удаление прошло успешно.");
        }
    }
}
