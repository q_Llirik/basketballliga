﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для GamesPage.xaml
    /// </summary>
    public partial class GamesPage : Page
    {
        public GamesPage()
        {
            InitializeComponent();

            UpDateListView();
        }

        private void AddNewGame_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new AddGamePage());
        }

        private void UpDateListView()
        {
            lvGames.Items.Clear();
            lvGames.ItemsSource = DataBase.db.Games.ToList();
        }

        private void DeleteGame_Click(object sender, RoutedEventArgs e)
        {
            if (lvGames.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите соревнование для удаления.");
                return;
            }

            var selectGame = (Games)lvGames.SelectedValue;
            DataBase.db.TeamsInGame.RemoveRange(DataBase.db.TeamsInGame.Where(w=>w.GameID == selectGame.ID));
            DataBase.db.Games.Remove(selectGame);

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            UpDateListView();
            MessageManager.ShowGoodMessage("Удаление прошло успешно.");
        }
    }
}
