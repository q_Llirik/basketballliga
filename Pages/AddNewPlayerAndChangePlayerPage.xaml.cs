﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddNewPlayerAndChangePlayerPage.xaml
    /// </summary>
    public partial class AddNewPlayerAndChangePlayerPage : Page
    {
        private bool ChangeMode = false;
        private Players changePlayer;

        public AddNewPlayerAndChangePlayerPage()
        {
            InitializeComponent();

            ChangeMode = false;

            cbxTeam.ItemsSource = DataBase.db.Teames.ToList();
            cbxTeam.SelectedIndex = 0;
            cbxTeam.DisplayMemberPath = "Name";

            dpDateOfBirth.SelectedDate = DateTime.Now;
        }

        public AddNewPlayerAndChangePlayerPage(Players Player)
        {
            InitializeComponent();

            ChangeMode = true;
            changePlayer = Player;

            tbxFullName.Text = changePlayer.FullName;
            dpDateOfBirth.SelectedDate = changePlayer.DateOfBirth;
            tbxGrowth.Text = changePlayer.Growth + "";
            tbxWeight.Text = changePlayer.Weight + "";
            tbxGameNumber.Text = changePlayer.GameNumber + "";
            tbxRole.Text = changePlayer.Role;
            cbxTeam.ItemsSource = DataBase.db.Teames.ToList();
            cbxTeam.SelectedItem = changePlayer.Teames;
            cbxTeam.DisplayMemberPath = "Name";
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new PlayersPage());
        }

        private void SaveOrChange_Click(object sender, RoutedEventArgs e)
        {
            if (tbxFullName.Text.Length == 0 || tbxGameNumber.Text.Length == 0 || tbxGrowth.Text.Length == 0 || tbxRole.Text.Length == 0 || tbxWeight.Text.Length == 0)
            {
                MessageManager.ShowErrorMessage("Введите все необходимые данные.");
                return;
            }

            var checkRegex = @"\A[0-9]{1,3}(?:[,][0-9])?\z";
            if (!Regex.IsMatch(tbxGrowth.Text, checkRegex) || !Regex.IsMatch(tbxWeight.Text, checkRegex) || !Regex.IsMatch(tbxGameNumber.Text, @"\d"))
            {
                MessageManager.ShowErrorMessage("Неправильный ввод числовых полей.");
                return;
            }

            var message = "";
            if (ChangeMode)
            {
                changePlayer.Teames = (Teames)cbxTeam.SelectedItem;
                changePlayer.FullName = tbxFullName.Text;
                changePlayer.DateOfBirth = dpDateOfBirth.SelectedDate.Value;
                changePlayer.Growth = float.Parse(tbxGrowth.Text);
                changePlayer.Weight = float.Parse(tbxWeight.Text);
                changePlayer.GameNumber = int.Parse(tbxGameNumber.Text);
                changePlayer.Role = tbxRole.Text;
                message = "Изменения игрока прошли успешно.";
            }
            else
            {
                var player = new Players() {
                    Teames = (Teames)cbxTeam.SelectedItem,
                    FullName = tbxFullName.Text,
                    DateOfBirth = dpDateOfBirth.SelectedDate.Value,
                    Growth = float.Parse(tbxGrowth.Text),
                    Weight = float.Parse(tbxWeight.Text),
                    GameNumber = int.Parse(tbxGameNumber.Text),
                    Role = tbxRole.Text
                };
                DataBase.db.Players.Add(player);
                message = "Добавление нового игрока прошло успешно.";
            }

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            MessageManager.ShowGoodMessage(message);
            Back_Click(null,null);
        }
    }
}
