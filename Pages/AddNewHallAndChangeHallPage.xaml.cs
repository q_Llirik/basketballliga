﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddNewHallAndChangeHallPage.xaml
    /// </summary>
    public partial class AddNewHallAndChangeHallPage : Page
    {
        private bool ChangeMode = false;
        private Halls changeHall;

        public AddNewHallAndChangeHallPage()
        {
            InitializeComponent();

            ChangeMode = false;

            cbxTeams.ItemsSource = DataBase.db.Teames.ToList();
            cbxTeams.DisplayMemberPath = "Name";
            cbxTeams.SelectedIndex = 0;
        }

        public AddNewHallAndChangeHallPage(Halls Hall)
        {
            InitializeComponent();

            ChangeMode = true;
            changeHall = Hall;

            tbxName.Text = changeHall.Name;
            tbxAddress.Text = changeHall.Address;
            tbxPhone.Text = changeHall.Phone;
            cbxTeams.ItemsSource = DataBase.db.Teames.ToList();
            cbxTeams.DisplayMemberPath = "Name";
            cbxTeams.SelectedItem = Hall.Teames;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new HallsPage());
        }

        private void SaveOrChange_Click(object sender, RoutedEventArgs e)
        {
            if (tbxName.Text.Length == 0 || tbxPhone.Text.Length == 0 || tbxAddress.Text.Length == 0)
            {
                MessageManager.ShowErrorMessage("Введите все необходимые данные.");
                return;
            }

            if (tbxPhone.Text.First() != '8' || !Regex.IsMatch(tbxPhone.Text, @"\d{11}"))
            {
                MessageManager.ShowErrorMessage("Номер телефона не соответствует формату {89997776655}.");
                return;
            }

            var message = "";
            if (ChangeMode)
            {
                changeHall.Name = tbxName.Text;
                changeHall.Address = tbxAddress.Text;
                changeHall.Phone = tbxPhone.Text;
                changeHall.Teames = (Teames)cbxTeams.SelectedItem;
                message = "Изменение зала прошло успешно.";
            }
            else
            {
                var hall = new Halls()
                {
                    Name = tbxName.Text,
                    Address = tbxAddress.Text,
                    Phone = tbxPhone.Text,
                    Teames = (Teames)cbxTeams.SelectedItem
                };

                DataBase.db.Halls.Add(hall);
                message = "Добавление нового зала прошло успешно.";
            }

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            MessageManager.ShowGoodMessage(message);
            Back_Click(null,null);
        }
    }
}
