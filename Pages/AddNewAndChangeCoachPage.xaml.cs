﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddNewAndChangeCoachPage.xaml
    /// </summary>
    public partial class AddNewAndChangeCoachPage : Page
    {
        private bool ChangeMode = false;
        private Coaches changeCoach;

        public AddNewAndChangeCoachPage()
        {
            InitializeComponent();

            ChangeMode = false;

            cbxTeams.ItemsSource = DataBase.db.Teames.ToList();
            cbxTeams.DisplayMemberPath = "Name";
            cbxTeams.SelectedIndex = 0;
        }

        public AddNewAndChangeCoachPage(Coaches Coach)
        {
            InitializeComponent();

            ChangeMode = true;
            changeCoach = Coach;

            tbxFullName.Text = changeCoach.FullName;
            tbxRank.Text = changeCoach.Rank;
            cbxTeams.ItemsSource = DataBase.db.Teames.ToList();
            cbxTeams.DisplayMemberPath = "Name";
            cbxTeams.SelectedItem = changeCoach.Teames;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new CoachesPage());
        }

        private void SaveOrChange_Click(object sender, RoutedEventArgs e)
        {
            if (tbxFullName.Text.Length == 0 || tbxRank.Text.Length == 0)
            {
                MessageManager.ShowErrorMessage("Введите необходимые данные.");
                return;
            }

            var message = "";
            if (ChangeMode)
            {
                changeCoach.FullName = tbxFullName.Text;
                changeCoach.Rank = tbxRank.Text;
                changeCoach.Teames = (Teames)cbxTeams.SelectedValue;
                message = "Изменения прошли успешно.";
            }
            else
            {
                var coach = new Coaches()
                {
                    FullName = tbxFullName.Text,
                    Rank = tbxRank.Text,
                    Teames = (Teames)cbxTeams.SelectedValue
                };
                DataBase.db.Coaches.Add(coach);
                message = "Добавление нового тренера прошло успешно.";
            }

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            MessageManager.ShowGoodMessage(message);
            Back_Click(null,null);
        }
    }
}
