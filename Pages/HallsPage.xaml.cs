﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для HallsPage.xaml
    /// </summary>
    public partial class HallsPage : Page
    {
        public HallsPage()
        {
            InitializeComponent();

            UpDateListView();
        }

        private void UpDateListView()
        {
            lvHalls.Items.Clear();
            lvHalls.ItemsSource = DataBase.db.Halls.ToList();
        }

        private void AddNewHall_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new AddNewHallAndChangeHallPage());
        }

        private void ChangeHall_Click(object sender, RoutedEventArgs e)
        {
            if (lvHalls.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите изменяемый зал.");
                return;
            }

            Navigation.OpenPage(new AddNewHallAndChangeHallPage((Halls)lvHalls.SelectedItem));
        }

        private void DeleteHall_Click(object sender, RoutedEventArgs e)
        {
            if (lvHalls.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите удалямый зал.");
                return;
            }

            var selectHall = (Halls)lvHalls.SelectedValue;
            DataBase.db.Halls.Remove(selectHall);

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            UpdateDefaultStyle();
            MessageManager.ShowErrorMessage("Удаление прошло успешно.");
        }
    }
}
