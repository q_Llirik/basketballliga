﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для PlayersPage.xaml
    /// </summary>
    public partial class PlayersPage : Page
    {
        public PlayersPage()
        {
            InitializeComponent();

            UpDateListView();
        }

        private void UpDateListView()
        {
            lvPlayers.Items.Clear();
            foreach (var player in DataBase.db.Players.ToList())
                lvPlayers.Items.Add(player);
        }

        private void DeleteHall_Click(object sender, RoutedEventArgs e)
        {
            if (lvPlayers.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите удаляемого игрока.");
                return;
            }

            DataBase.db.Players.Remove((Players)lvPlayers.SelectedItem);

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            UpDateListView();
            MessageManager.ShowGoodMessage("Удаление прошло успешно.");
        }

        private void ChangeHall_Click(object sender, RoutedEventArgs e)
        {
            if (lvPlayers.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите изменяемого игрока.");
                return;
            }

            Navigation.OpenPage(new AddNewPlayerAndChangePlayerPage((Players)lvPlayers.SelectedItem));
        }

        private void AddNewHall_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new AddNewPlayerAndChangePlayerPage());
        }
    }
}
