﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для CoachesPage.xaml
    /// </summary>
    public partial class CoachesPage : Page
    {
        public CoachesPage()
        {
            InitializeComponent();

            UpDateListView();
        }

        public void UpDateListView()
        {
            lvCoaches.Items.Clear();
            foreach (var coach in DataBase.db.Coaches.ToList())
                lvCoaches.Items.Add(coach);
        }

        private void ChangeCoach_Click(object sender, RoutedEventArgs e)
        {
            if (lvCoaches.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите изменяемого тренера.");
                return;
            }
            Navigation.OpenPage(new AddNewAndChangeCoachPage((Coaches)lvCoaches.SelectedItem));
        }

        private void AddNewCoach_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new AddNewAndChangeCoachPage());
        }

        private void DeleteCoach_Click(object sender, RoutedEventArgs e)
        {
            if (lvCoaches.SelectedIndex == -1)
            {
                MessageManager.ShowErrorMessage("Выберите удаляемого тренера.");
                return;
            }

            var selectCoach = (Coaches)lvCoaches.SelectedValue;
            DataBase.db.Coaches.Remove(selectCoach);

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            UpDateListView();
            MessageManager.ShowGoodMessage("Удаление прошло успешно.");
        }
    }
}
