﻿using BasketBallLiga.DB;
using BasketBallLiga.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BasketBallLiga.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddNewTeamAndChangeTeamPage.xaml
    /// </summary>
    public partial class AddNewTeamAndChangeTeamPage : Page
    {
        private bool ChangeMode = false;
        private Teames changeTeam;

        public AddNewTeamAndChangeTeamPage()
        {
            InitializeComponent();

            ChangeMode = false;

            dpDateMake.SelectedDate = DateTime.Now;
        }

        public AddNewTeamAndChangeTeamPage(Teames Team)
        {
            InitializeComponent();

            ChangeMode = true;
            changeTeam = Team;

            tbxName.Text = changeTeam.Name;
            dpDateMake.SelectedDate = changeTeam.MakeDate;
            tbxCity.Text = changeTeam.City;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Navigation.OpenPage(new TeamesPage());
        }

        private void SaveOrChange_Click(object sender, RoutedEventArgs e)
        {
            if (tbxName.Text.Length == 0 || tbxCity.Text.Length == 0)
            {
                MessageManager.ShowErrorMessage("Введите все необзодимые данные.");
                return;
            }

            var message = "";
            if (ChangeMode)
            {
                changeTeam.Name = tbxName.Text;
                changeTeam.MakeDate = dpDateMake.SelectedDate.Value;
                changeTeam.City = tbxCity.Text;
                message = "Изменение команды прошло успешно.";
            }
            else
            {
                var team = new Teames()
                {
                    Name = tbxName.Text,
                    MakeDate = dpDateMake.SelectedDate.Value,
                    City = tbxCity.Text
                };

                DataBase.db.Teames.Add(team);
                message = "Добавление новой команды прошло успешно.";
            }

            try
            {
                DataBase.db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageManager.ShowErrorMessage(ex.Message);
                return;
            }

            MessageManager.ShowGoodMessage(message);
            Back_Click(null,null);
        }
    }
}
