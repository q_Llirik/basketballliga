﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BasketBallLiga.Pages;
using BasketBallLiga.Tools;

namespace BasketBallLiga
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            this.Title = App.AppName;

            Navigation.main = this;
            Navigation.OpenPage(new GamesPage());
        }

        private void CoachesShow_MLBD(object sender, MouseButtonEventArgs e)
        {
            if (Container.Content.GetType().FullName == new CoachesPage().ToString()) return;

            Navigation.OpenPage(new CoachesPage());
        }

        private void GamesShow_MLBD(object sender, MouseButtonEventArgs e)
        {
            if (Container.Content.GetType().FullName == new GamesPage().ToString()) return;
            
            Navigation.OpenPage(new GamesPage());
        }

        private void HallsShow_MLBD(object sender, MouseButtonEventArgs e)
        {
            if (Container.Content.GetType().FullName == new HallsPage().ToString()) return;

            Navigation.OpenPage(new HallsPage());
        }

        private void TeamsShow_MLBD(object sender, MouseButtonEventArgs e)
        {
            if (Container.Content.GetType().FullName == new TeamesPage().ToString()) return;

            Navigation.OpenPage(new TeamesPage());
        }

        private void PlayersShow_MLBD(object sender, MouseButtonEventArgs e)
        {
            if (Container.Content.GetType().FullName == new PlayersPage().ToString()) return;

            Navigation.OpenPage(new PlayersPage());
        }
    }
}
